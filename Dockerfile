	FROM python:3.10-alpine

	ENV PYTHONUNBUFFERED=1
	ENV PYTHONDONTWRITEBYTECODE=1

	RUN apk add python3-dev postgresql-client postgresql-dev musl-dev build-base

	ADD requirements.txt /
	RUN pip install --upgrade pip
	RUN pip install -r requirements.txt

	WORKDIR /app/
	COPY . /app/

	COPY ./entrypoint.sh /
	ENTRYPOINT ["sh", "entrypoint.sh"]

