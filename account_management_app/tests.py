from django.test import TestCase, Client
from django.contrib.auth.models import User
from . models import Account, Customer


class AccountTestCase(TestCase):
    def test_initial(self):
        assert True

    def setUp(self):
        self.client = Client()
        self.credentials = {
            'username': 'user',
            'password': 'test123'
        }
        self.user = User.objects.create_user(username='user', email='user@dummy.com', password='test123')
        self.user.save()

        bank_user = User.objects.create_user(username='bank', email='bank@dummy.com', password='test123')
        bank_user.save()

        self.customer = Customer(user=self.user, phone_number='555666', rank='silver')
        self.customer.save()

        self.account = Account.objects.create(user=self.user, name='Account', is_customer=True)
        self.account_id = self.account.pk
        self.account.save()

        bank_account = Account.objects.create(user=bank_user, name='Account', is_customer=False)
        bank_account.save()

        self.client.post('/accounts/login/', self.credentials, follow=True)

    def test_get_loan(self):
        my_account = Account.objects.get(account_number=self.account_id)
        amount = 20
        bank_account = Account.objects.filter(is_customer=False)[0]
        bank_account.make_payment(amount, self.account_id, is_loan=True)
        balance = my_account.balance
        assert balance == 20

    def test_amount_owned(self):
        my_account = Account.objects.get(account_number=self.account_id)
        amount_owed = my_account.get_amount_owed()
        assert amount_owed == 0

    def test_get_all_accounts(self):
        accounts = Account.objects.filter(user=self.user)
        accounts_count = accounts.count()
        assert accounts_count == 1

    def test_total_balance(self):
        total_balance = self.user.customer.total_balance
        assert total_balance == 0

    def test_load_index(self):
        self.client.login(username='user', password='test123')
        response = self.client.get('/', follow=True)
        self.assertTemplateUsed(response, 'account_management_app/index.html')
        assert response.status_code == 200

    def test_account_details(self):
        self.client.login(username='user', password='test123')
        response = self.client.get('/account_details/1/', follow=True)
        self.assertTemplateUsed(response, 'account_management_app/account_details.html')
        assert response.status_code == 200
