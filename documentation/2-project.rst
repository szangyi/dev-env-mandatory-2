The Project
=========

Project description
-----------

The Bank App is an application written in the python framework Django. With the application, the user can keep track of their balance, transfer money externally as well as internally between his/her other accounts, and manage their account information. The account's data is stored in the relational database, postgresQL.