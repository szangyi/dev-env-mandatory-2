Software Quality Assurance
==========================



Coding standards
-------------------
All developers working on The Bank App must follow the coding standards defined by Django. Please read through the following documentation before you start work on the project. https://docs.djangoproject.com/en/dev/internals/contributing/writing-code/coding-style/


CI/CD pipeline
-------------------
Please follow the rules for continuous integration and continuous delivery for this project. This means that when a single feature is complete and functioning, all developers must commit their changes to the repository. Do not submit code that does not work. Each commit should include a comment explaining the added code and its purpose. A pipeline will be triggered every time a push is made to any branch. The pipeline will perform a unit test of your code as well as linting. For linting we are using Pylama, Djlint, and YAMLLint. If you do not pass the pipeline, please read the error messages provided and fix the bugs/errors. Once the errors are resolved, you can commit and push the code to the branch again.


Test usage
-------------------
We are using a Python standard library for unit testing. The test.py file will be run once the pipeline is triggered and will check if a quote will be created correctly without any errors. The tests will be described and explained in this section and a notice will be added at the top of this documentation, once they are implemented.


Linting
-------------------
All code will be analyzed with the help of a 3 linting tools: Pylama, Djlint, and YAMLLint. These plugins will analyze the stylistic parts of your code based on the Django coding standards.