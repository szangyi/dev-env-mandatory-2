The Bank App
================================================

.. _about:
This is a app created using Django and PostgreSQL running with Docker.


Quick start
-------------------------------------------

Running the application requires Docker to be installed locally. For installation guides, visit: https://docs.docker.com/installation/.

Clone this repository:

.. code-block::

   $ mkdir my-clone
   $ cd my-clone/
   $ git clone https://gitlab.com/szangyi/dev-env-mandatory-2.git



Run the project in Docker's virtual enviroment, use the relevant RTE profile depending on the purpose of your use:

.. code-block::

   $ RTE=dev docker-compose up      # DEVELOPMENT
   OR
   $ RTE=test docker-compose up     # TESTING
   OR
   $ RTE=prod docker-compose up     # PRODUCTION
