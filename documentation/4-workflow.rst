Workflow
=========

Every time you are working on a new feature for the project, create a new branch.

.. code-block::

   $ git branch <new_branch_name>


When a change is made and the code is completed, make sure to commit your changes to the branch.

.. code-block::

   $ git add .
   $ git commit -m “<A relevant comment about the new feature or fix>”


Once you code is committed, push the code to your branch

.. code-block::

   $ git push

If it is the first time you push to your branch, make sure to set the upstream branch in order to make it work

.. code-block::

   $ git push --set-upstream <remote> <branch>


Finally, make a merge request.