from django.test import TestCase
from django.contrib.auth.models import User


class LoginTestCase(TestCase):
    def test_initial(self):
        print("Starting to run test in Login app")
        assert True

    def test_login_view(self):
        response = self.client.get(path='/accounts/login/')
        assert response.status_code == 200
        assert "Login" in str(response.content)

    def setUp(self):
        self.credentials = {
            'username': 'user',
            'password': 'test123'
        }
        self.admin_credentials = {
            'username': 'employee',
            'password': 'test123'
        }
        User.objects.create_user(**self.credentials)
        User.objects.create_user(**self.admin_credentials)

    def test_login(self):
        response = self.client.post('/accounts/login', self.credentials, follow=True)
        assert response.status_code == 200

    def test_employee_login(self):
        response = self.client.post('/accounts/login/', self.admin_credentials, follow=True)
        assert response.status_code == 200
